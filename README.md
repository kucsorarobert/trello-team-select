Simple bookmarklet to select cards owned by specific people on a Trello board.

1. Add the Trello ID of the people to the 'member' array.
2. Create a bookmarklet e.g. with http://mrcoles.com/bookmarklet/
3. Navigate to the Trello board and use the bookmarklet.
4. Profit!
